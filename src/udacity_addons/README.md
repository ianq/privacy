# Udacity Addons

- contains extensions on the Udacity course, specifically me working through recommended work

## Differential Privacy for DL

1) Exponential Mechanism

2) Moment's Accountant

3) Differentially Private SGD

## Papers:

Below are papers that are related to DP but I've uploaded them to my personal Fermats Library account to track my notes

1) [My Notes: Semi-Supervised Knowledge Transfer for Deep Learning from Private Training Data](https://www.fermatslibrary.com/p/63970083)

2) [My Notes: Deep Learning with Differential Privacy](https://www.fermatslibrary.com/p/df5e159c)