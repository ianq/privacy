# Installations

```
conda create -n pysyft
conda install pytorch torchvision cpuonly -c pytorch
conda install jupyter notebook
pip install zstd  # Might need to remove $HOME/.conda/envs/pysyft/compiler_compat/ld if on Arch
pip install "syft[udacity]"
```

**Note** tried to get it working on a Mac but gave up. You're on your own, friendo

# OpenMined
Fucking around with Privacy ML stuff via OpenMined

# udacity
Materials from the Udacity class [Secure and Private AI](https://classroom.udacity.com/courses/ud185#) as well as my own deep dives from the recommendations

# intro-to-pytorch 

Additional material that goes along with the udacity class
